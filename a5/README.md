fail> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Moblie Web Application Development 

## Rose Pennington

### Assignment 5 Requirements:

*Includes:*

1. Basic server-side validation 
2. Passed Validation
3. Failed Validation 
4. Perpared statmenets to help prevent SQL injection
5. JSTL to prevent XSS
6. Insert functionality to A4

#### README.md file should include the following items:

* Assessment links 

1. [http://localhost:9999/lis4368](http://localhost:9999/lis4368)

#### Assignment Screenshots:


*Screenshot of Failed Validation*:

![fail](img/fail5.png)

*Screenshot of Passed Validation*:

![passed](img/passed5.png)

*Screenshot of Associated Database Entry*:

![passed](img/data5.png)



