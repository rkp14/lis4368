> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Moblie Web Application Development 

## Rose Pennington

### Assignment 4 Requirements:

*Includes:*

1. Server-side passed validation 
2. Server-side failed validation
3. Java File Compilation

#### README.md file should include the following items:

* Assessment links 

1. [http://localhost:9999/lis4368](http://localhost:9999/lis4368)

#### Assignment Screenshots:


*Screenshot of Failed Validation*:

![fail](img/fail4.png)

*Screenshot of Passed Validation*:

![passed](img/passed4.png)



