> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4381

## Rose Pennington

### Assignment Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install tomcat
    - Bitbucket account
    - git commands
 

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Download and Install MySQL
	- Develop and Deply a WebApp
	- Deploy Servlet 

3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Pet's R-Us business ERD 
	- 10 records per table
	- Forward-engineered 
	- Links to .sql and .mwb files 

4. [P1 README.md](p1/README.md "My P1 README.md file")
	- Clone Bitbucket repo
	- Modify index.jsp 
	- Write client-side validation 
	- Push to Bitbucket 

5. [A4 README.md](a4/README.md "My A4 README.md file")
	- Compile Class Files  
	- Server-Side Validation 
	- Push files to repository 

6. [A5 README.md](a5/README.md "My A5 README.md file")
	- Basic Server-Side Validation 
	- Prevent SQL injection
	- JSTL to prevent XSS
	- Add insert functioanlity to A4 

7. [P2 README.md](p2/README.md "My P2 README.md file")
	- MVC framework
	- Using basic client-server side validation
	- Completes CRUD functionality 


	