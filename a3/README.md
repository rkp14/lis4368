> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Moblie Web Application Development 

## Rose Pennington

### Assignment 3 Requirements:

*Includes:*

1. Pet's R-Us business forward-engineered ERD with 10 records per table
2. Links to a3.mwb and a3.sql 

#### README.md file should include the following items:

* Assessment links 

1. [a3.mwb](docs/a3.mwb)

2. [a3.sql](docs/a3.sql)


#### Assignment Screenshots:

*Screenshot of Pets-R-US ERD*:

![ERD](img/ERD3.png)



