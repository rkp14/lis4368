> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Moblie Web Application Development 

## Rose Pennington

### Project 2 Requirements:

*Includes:*

1. MVC framework
2. Using basic client-server side validation
3. Completes CRUD functionality 

#### README.md file should include the following items:

* Assessment links 

1. [http://localhost:9999/lis4368](http://localhost:9999/lis4368)

#### Assignment Screenshots:

*Screenshot of Passed Validation*:

![passed](img/valid_entry.png)

*Screenshot of Display Data*:

![passed](img/display_data.png)

*Screenshot of Display Data after Editing*:

![passed](img/display_edit.png)

*Screenshot of Delete Warning*:

![passed](img/delete.png)

*Screenshot of Database Changes*:

![passed](img/database.png)



