> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Moblie Web Application Development 

## Rose Pennington

### Project 1 Requirements:

*Includes:*

1. Client-side validation 
2. My Online Profile
3. Main/Splash Page with new photo 

#### README.md file should include the following items:

* Assessment links 

1. [http://localhost:9999/lis4368](http://localhost:9999/lis4368)

#### Assignment Screenshots:


*Screenshot of Failed Validation*:

![fail](img/fail.png)

*Screenshot of Passed Validation*:

![passed](img/passed.png)



