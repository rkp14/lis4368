> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Moblie Web Application Development 

## Rose Pennington

### Assignment 2 Requirements:

*Includes:*

1. Download and install MySQL
2. Create new user 
3. Develop and Deply a WebApp 
4. Deploying Servlet 

#### README.md file should include the following items:

* Assessment links 

1. [http://localhost:9999/hello](http://localhost:9999/hello)

2. [http://localhost:9999/hello/index.html](http://localhost:9999/hello/index.html)

3. [http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello)

4. [http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html)

5. [http://localhost:9999/hello/sayhi](http://localhost:9999/hello/sayhi)

* Screenshot of the query results from the following links 

#### Assignment Screenshots:

*Screenshot of http://localhost:9999/hello*:

![Hello](img/hello.png)

*Screenshot of index*:

![index](img/index.png)

*Screenshot of sayhello*:

![Say Hello](img/sayhello.png)

*Screenshot of querybook*:

![querybook](img/querybook.png)

*Screenshot of sayhi*:

![sayhi](img/sayhi.png)

*Screenshot of query_selected*:

![index](img/query_selected.png)

*Screenshot of query results*:

![index](img/query_results.png)


