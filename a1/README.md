> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Moblie Web Application Development 

## Rose Pennington

### Assignment 1 Requirements:

*Includes:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations 
3. Chapter Questions (Ch 1, 2)
4. Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbutcketsationloactions and myteamquotes).

#### README.md file should include the following items:

* Screenshot of AMPPS Installation [My PHP Installation](http://localhost:9999 "PHP Localhost");
* Screenshot of running java Hello;
* Screenshof of running Android Studio - My First App 
* git commands w/short descriptions; 


> #### Git commands w/short descriptions:

1. git init- create an empty git repository or reinitialize an existing one 
2. git status- checks on status of the file 
3. git add- registers the file for committing the file to the repository 
4. git commit-  commits file to the repository 
5. git push- publish file to the remote repository 
6. git pull- fetch from and integrate with another repository or local branch 
7. git clone- checkout the source code 


#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/jdk_install.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/tomcat.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/rkp14/bucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/rkp14/myteamquotes/ "My Team Quotes Tutorial")
